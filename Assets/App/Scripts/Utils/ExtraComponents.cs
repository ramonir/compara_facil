﻿using TMPro;
using UnityEngine.UI;

public class ExtraComponents
{
    [System.Serializable]
    public class RequiredField
    {
        public TMP_InputField inputF;
        public TextMeshProUGUI txt_warning;
    }

    [System.Serializable]
    public class FieldMoreLess
    {
        public TMP_InputField inputF;
        private int minValue;
        private Button btn_more;
        private Button btn_less;

        public FieldMoreLess(TMP_InputField new_inputF, int new_minValue)
        {
            inputF = new_inputF;
            minValue = new_minValue;

            btn_more = inputF.gameObject.transform.GetChild(1).GetComponent<Button>();
            btn_less = inputF.gameObject.transform.GetChild(2).GetComponent<Button>();

            inputF.onEndEdit.AddListener(delegate { AntNull(inputF); });

            btn_more.onClick.AddListener(() => FieldMoreAction(inputF));
            btn_less.onClick.AddListener(() => FieldLessAction(inputF));
        }

        void FieldMoreAction(TMP_InputField inputF)
        {
            int value = int.Parse(inputF.text);
            value++;
            inputF.text = value.ToString();
        }

        void FieldLessAction(TMP_InputField inputF)
        {
            int value = int.Parse(inputF.text);
            value--;
            if (value < minValue) return;
            inputF.text = value.ToString();
        }

        void AntNull(TMP_InputField inputF)
        {
            if (inputF.text == "") inputF.text = minValue.ToString();
        }
    }
}
