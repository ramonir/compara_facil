﻿using TMPro;
using System.Globalization;
using UnityEngine;

public static class UtilityHelper
{
    public static void AdjustInputFieldToReal(TMP_InputField inputF)
    {
        if (inputF.text == "") return;
        float value = AdjustStringRealToFloat(inputF.text);
        if (value < 1f) value.ToString("0.00");
        else inputF.text = value.ToString("#.00");
    }

    public static string AdjustStringToReal(string text)
    {
        if (text == "") return text;
        float value = AdjustStringRealToFloat(text);
        if (value < 1f) return value.ToString("0.00");
        else return text = value.ToString("#.00");
    }

    public static float AdjustStringRealToFloat(string txt)
    {
        string txt_new = txt.Replace(",", ".");
        float toFloat = float.Parse(txt_new, CultureInfo.InvariantCulture);
        return toFloat;
    }

    public static bool ValidateInputField(TMP_InputField inputF, TextMeshProUGUI txt_warning)
    {
        if (inputF.text == "")
        {
            txt_warning.enabled = true;
            inputF.image.color = Color.red;
            return false;
        }
        else
        {
            txt_warning.enabled = false;
            inputF.image.color = Color.gray;
            return true;
        }
    }
}
