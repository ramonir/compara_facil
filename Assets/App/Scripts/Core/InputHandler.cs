using UnityEngine;
using App.Commands;

namespace App.Core
{
    public class InputHandler
    {
        private Command _escapeCommand;
        public Command EscapeCommand { get => _escapeCommand; }

        public InputHandler()
        {
            this._escapeCommand = new EscapeCommand();
        }


        public Command Handle()
        {
            if (Input.GetKeyUp("escape")) return this.EscapeCommand;

            return null; //TODO: add input logics to return command
        }
    }
}