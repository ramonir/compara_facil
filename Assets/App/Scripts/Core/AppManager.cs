
namespace App.Core
{
    public class AppManager : BaseManager<AppManager>
    {
        private InputHandler _input;
        public InputHandler Input { get => _input; }

        protected AppManager()
        {
            // base.Awake();

            this._input = new InputHandler();
        }

        public void Update()
        {
            this._input?.Handle()?.Execute();
        }
    }
}