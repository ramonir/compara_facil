using UnityEngine;

namespace App.Core
{
    public enum CurrentScreen { Home, Dev, Features }
    public class ScreenManager : BaseManager<ScreenManager>
    {
        [SerializeField] private RectTransform can; //Main Canvas
        [SerializeField] private int speedScreen;
        private CurrentScreen currentScreen;

        private ScreenHome _screen_home;
        public ScreenHome Screen_Home { get => _screen_home; }
        private ScreenFeatures _screen_features;
        public ScreenFeatures Screen_Features { get => _screen_features; }
        private ScreenDev _screen_dev;
        public ScreenDev Screen_Dev { get => _screen_dev; }

        private NewProduct _newProduct;
        public NewProduct NewProduct { get => _newProduct; set => _newProduct = value; }
        
        private Price _price;
        public Price Price { get => _price; set => _price = value; }

        private Points _points;
        public Points Points { get => _points; set => _points = value; }

        void Awake()
        {
            this._screen_home = FindObjectOfType<ScreenHome>();
            this._screen_features = FindObjectOfType<ScreenFeatures>();
            this._screen_dev = FindObjectOfType<ScreenDev>();
        }

        public void OnEnable()
        {
            AppManager.Instance.Input.EscapeCommand.OnCommand += OnEscape;
        }

        public void OnDisable()
        {
            AppManager.Instance.Input.EscapeCommand.OnCommand -= OnEscape;
        }

        void Update()
        {
            Transition();
        }

        public void ChangeScreen(CurrentScreen nextScreen)
        {
            currentScreen = nextScreen;
        }

        void Transition()
        {
            switch (currentScreen)
            {
                case CurrentScreen.Home:
                    {
                        _screen_features.pos = Vector2.Lerp(_screen_features.pos, new Vector2(can.rect.width, _screen_features.pos.y),
                            speedScreen * Time.deltaTime);

                        _screen_dev.pos = Vector2.Lerp(_screen_dev.pos, new Vector2(_screen_dev.pos.x, _screen_dev.startPos.y),
                            speedScreen * Time.deltaTime);
                        break;
                    }
                case CurrentScreen.Features:
                    {
                        _screen_features.pos = Vector2.Lerp(_screen_features.pos, new Vector2(0, 0),
                            speedScreen * Time.deltaTime);

                        _screen_dev.pos = Vector2.Lerp(_screen_dev.pos, new Vector2(_screen_dev.pos.x, -can.rect.height / 2),
                            speedScreen * Time.deltaTime);
                        break;
                    }
                case CurrentScreen.Dev:
                    {
                        _screen_features.pos = Vector2.Lerp(_screen_features.pos, new Vector2(can.rect.width, _screen_features.pos.y),
                            speedScreen * Time.deltaTime);

                        _screen_dev.pos = Vector2.Lerp(_screen_dev.pos, new Vector2(0, 0),
                            speedScreen * Time.deltaTime);
                        break;
                    }
            }
            // _screen_funcs.Screen.anchoredPosition = _screen_funcs.Pos;
            // _screen_dev.Screen.anchoredPosition = _screen_dev.Pos;
        }

        void OnEscape()
        {
            if (currentScreen == CurrentScreen.Home) Application.Quit();
            else _screen_home.BackToHome();
        }
    }
}
