﻿using App.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FeatureButton : MonoBehaviour
{
    private int id;
    private RectTransform rectT;
    private Button btn_main;
    private TextMeshProUGUI txt_title;
    private TextMeshProUGUI txt_desc;
    private Button btn_info; 
    private RectTransform feature;

    void Awake()
    {
        rectT = GetComponent<RectTransform>();
        btn_main = rectT.GetChild(0).GetComponent<Button>();

        txt_title = btn_main.transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
        txt_desc = btn_main.transform.GetChild(1).GetComponent<TextMeshProUGUI>();

        btn_main.onClick.AddListener(ChooseFeature);
    }

    public void SetItems(int id, string title, string desc, RectTransform feature){
        this.id = id;
        txt_title.text = title;
        txt_desc.text = desc;
        this.feature = feature;

        AdjustProductPosition();
    }

    void AdjustProductPosition()
    {
        rectT.anchoredPosition = new Vector2(0, id * -ScreenManager.Instance.Screen_Home.featureButtonDistance); // Change Position
    }

    void ChooseFeature(){
        ScreenManager.Instance.Screen_Home.BlockHomeTouch(true);
        ScreenManager.Instance.Screen_Features.DestroyCurrentFeature(feature);
        ScreenManager.Instance.Screen_Features.StartNewFeature(feature);
    }
}
