﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;
using UnityEngine.UI;

public class ScreenDev : ScreenApp
{
    public Vector2 startPos;
    [SerializeField] private Button btn_helpDev;
    [SerializeField] private Button btn_back;
    [SerializeField] private Button btn_picPay;
    [SerializeField] private Button btn_pagSeguro;
    const string URL_PICPAY = "http://picpay.me/ramonir";
    const string URL_PAGSEGURO = "https://pag.ae/7VVirHTr3";

    protected override void Awake()
    {
        base.Awake();
        startPos = pos;

        btn_picPay.onClick.AddListener(() => Application.OpenURL(URL_PICPAY));
        btn_pagSeguro.onClick.AddListener(() => Application.OpenURL(URL_PAGSEGURO));
    }

    void Start()
    {
        btn_helpDev.onClick.AddListener(ShowScreenDev);
        btn_back.onClick.AddListener(ScreenManager.Instance.Screen_Home.BackToHome);
    }

    void ShowScreenDev()
    {
        ScreenManager.Instance.Screen_Home.BlockHomeTouch(true);
        ScreenManager.Instance.ChangeScreen(CurrentScreen.Dev);
    }
}
