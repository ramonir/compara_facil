﻿using UnityEngine;
public abstract class ScreenApp : MonoBehaviour
{
    private RectTransform screen;
    public Vector2 pos;

    protected virtual void Awake()
    {
        this.screen = GetComponent<RectTransform>();
        this.pos = screen.anchoredPosition; //Debug para telas não irem para o MainCanvas
    }

    private void FixedUpdate() {
        screen.anchoredPosition = pos;
    }

    // public abstract void Transition();
}
