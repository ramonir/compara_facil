﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using UnityEngine;
using UnityEngine.UI;

public class ScreenFeatures : ScreenApp
{    
    RectTransform panel_features;
    [SerializeField] private Button btn_back;
    RectTransform rectT_currentFeature;

    void Start()
    {
        panel_features = transform.GetChild(0).GetComponent<RectTransform>();

        btn_back.onClick.AddListener(ScreenManager.Instance.Screen_Home.BackToHome);
    }

    public void DestroyCurrentFeature(RectTransform rectT_otherFeature){
        if(rectT_currentFeature == rectT_otherFeature) return;
        rectT_currentFeature = null;
        foreach (RectTransform child in panel_features)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public void StartNewFeature(RectTransform rectT_otherFeature){
        ScreenManager.Instance.ChangeScreen(CurrentScreen.Features);
        if(rectT_currentFeature != null) return;
        Instantiate(rectT_otherFeature, panel_features.transform.position, panel_features.transform.rotation, panel_features);
        rectT_currentFeature = rectT_otherFeature;
    }
}
