﻿using App.Core;
using UnityEngine;
using UnityEngine.UI;

public class ScreenHome : ScreenApp
{
    [System.Serializable]
    public class FeatureButtonItems
    {
        public int id;
        public string title;
        public string desc;
        public string path;
    }
    private ScrollRect scroll_buttons;
    private RectTransform point_buttons;
    private RectTransform prefab_featureButton;
    private FeatureButtonItems[] arr_featureButtonItems;
    [SerializeField] private Button btn_shadow;

    [Header("Infos")]
    public int featureButtonDistance = 400;

    void Start()
    {
        scroll_buttons = GetComponentInChildren<ScrollRect>();
        point_buttons = scroll_buttons.content.GetChild(0).GetComponent<RectTransform>();
        prefab_featureButton = (Resources.Load("Prefabs/featureButton") as GameObject).GetComponent<RectTransform>();

        btn_shadow.onClick.AddListener(BackToHome);

        FindFeatureButtons();
    }

    void FindFeatureButtons()
    {
        string json = (Resources.Load("Json/json_featureButtons") as Object).ToString();
        arr_featureButtonItems = JsonHelper.FromJson<FeatureButtonItems>(json);
        AddNewFeatureButtons(arr_featureButtonItems);
    }

    void AddNewFeatureButtons(FeatureButtonItems[] arr_item)
    {
        for (int i = 0; i < arr_item.Length; i++)
        {
            var gO_featureButton = Instantiate(prefab_featureButton, point_buttons.transform.position, point_buttons.transform.rotation, point_buttons);
            var featureButton = gO_featureButton.GetComponent<FeatureButton>();
            
            var feature = (Resources.Load(arr_item[i].path) as GameObject).GetComponent<RectTransform>();
            featureButton.SetItems(arr_item[i].id, arr_item[i].title, arr_item[i].desc, feature);
        }

        AdjustScrollSize();
    }

    void AdjustScrollSize()
    {
        var scrollExtraSize = 100;
        scroll_buttons.content.sizeDelta = new Vector2(scroll_buttons.content.sizeDelta.x, (arr_featureButtonItems.Length * featureButtonDistance) + scrollExtraSize);
    }

    public void BackToHome(){
        ScreenManager.Instance.ChangeScreen(CurrentScreen.Home);
        BlockHomeTouch(false);
    }

    public void BlockHomeTouch(bool value)
    {
        btn_shadow.gameObject.SetActive(value);
    }
}
