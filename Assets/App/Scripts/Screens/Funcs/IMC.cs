﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IMC : MonoBehaviour
{
    [System.Serializable]
    public class Table
    {
        public RectTransform rectT;
        public List<Image> list_img_column;
    }

    [Header("Panel Top")]
    [SerializeField] private ExtraComponents.RequiredField reqF_height;
    [SerializeField] private ExtraComponents.RequiredField reqF_weight;
    [SerializeField] private Button btn_calc;
    [SerializeField] private Table tab_imc;

    [Header("Panel Bot")]
    [SerializeField] private RectTransform panel_bot;
    [SerializeField] private TextMeshProUGUI txt_resultTitle;
    [SerializeField] private TextMeshProUGUI txt_resultIMC;

    const string IMC_HEIGHT = "IMC_HEIGHT";
    const string IMC_WEIGHT = "IMC_WEIGHT";

    void Start()
    {
        reqF_height.inputF.onEndEdit.AddListener(delegate { AdjustInputFieldToHeight(reqF_height.inputF); });

        btn_calc.onClick.AddListener(Calc);

        GetTableList(tab_imc);

        RequestData();
    }

    void GetTableList(Table tab)
    {
        var amount = tab.rectT.childCount;
        for (int i = 1; i < amount; i++)
        {
            tab.list_img_column.Add(tab.rectT.GetChild(i).GetComponent<Image>());
        }
    }

    void RequestData()
    {
        if (PlayerPrefs.HasKey(IMC_HEIGHT)) reqF_height.inputF.text = PlayerPrefs.GetFloat(IMC_HEIGHT).ToString(CultureInfo.InvariantCulture);
        AdjustInputFieldToHeight(reqF_height.inputF);
        if (PlayerPrefs.HasKey(IMC_WEIGHT)) reqF_weight.inputF.text = PlayerPrefs.GetFloat(IMC_WEIGHT).ToString(CultureInfo.InvariantCulture);
    }

    void Calc()
    {
        if(!UtilityHelper.ValidateInputField(reqF_height.inputF, reqF_height.txt_warning)) return;
        if(!UtilityHelper.ValidateInputField(reqF_weight.inputF, reqF_weight.txt_warning)) return;

        panel_bot.gameObject.SetActive(true);

        float value_height = float.Parse(reqF_height.inputF.text, CultureInfo.InvariantCulture);
        float value_weight = float.Parse(reqF_weight.inputF.text, CultureInfo.InvariantCulture);

        float imc = value_weight / (value_height * value_height);

        Color color_column;
        Color color_result;
        int id = 0;
        if (imc < 18.5f)
        {
            ColorUtility.TryParseHtmlString("#FFD25A", out color_column); //Amarelo soft
            ColorUtility.TryParseHtmlString("#FF9600", out color_result); //Amarelo
            id = 0;
        }
        else if (imc >= 18.5f && imc <= 24.9f)
        {
            ColorUtility.TryParseHtmlString("#5AFF5A", out color_column); //Verde soft
            ColorUtility.TryParseHtmlString("#00B400", out color_result); //Verde
            id = 1;
        }
        else if (imc >= 25f && imc <= 29.9f)
        {
            ColorUtility.TryParseHtmlString("#FFD25A", out color_column); //Amarelo soft
            ColorUtility.TryParseHtmlString("#FF9600", out color_result); //Amarelo
            id = 2;
        }
        else if (imc >= 30f && imc <= 39.9f)
        {
            ColorUtility.TryParseHtmlString("#FF8C5A", out color_column); //Laranja soft
            ColorUtility.TryParseHtmlString("#FF5202", out color_result); //Laranja
            id = 3;
        }
        else
        {
            ColorUtility.TryParseHtmlString("#FF2D2D", out color_column); //Red soft
            ColorUtility.TryParseHtmlString("#C40000", out color_result); //Red
            id = 4;
        }
        for (int i = 0; i < tab_imc.list_img_column.ToArray().Length; i++) tab_imc.list_img_column[i].color = Color.white;
        tab_imc.list_img_column[id].color = color_column;

        var idealWeightMin = (value_height * value_height) * 18.5f;
        var idealWeightMax = (value_height * value_height) * 24.9f;

        txt_resultTitle.text = $"O seu peso ideal pode variar entre <b>{idealWeightMin.ToString("#.0", CultureInfo.InvariantCulture)}kg</b> e <b>{idealWeightMax.ToString("#.0", CultureInfo.InvariantCulture)}kg</b>";

        txt_resultIMC.text = $"{imc.ToString("00.0", CultureInfo.InvariantCulture)}";
        txt_resultIMC.color = color_result;

        SaveData(value_height, value_weight);
    }

    void AdjustInputFieldToHeight(TMP_InputField inputF)
    {
        if (inputF.text == "") return;
        string txt_new = inputF.text.Replace(".", "");
        float value = float.Parse(txt_new, CultureInfo.InvariantCulture);
        if (value >= 30)
        {
            value /= 100;
            inputF.text = value.ToString("0.00", CultureInfo.InvariantCulture);
        }
        else
        {
            value /= 10;
            inputF.text = value.ToString("0.00", CultureInfo.InvariantCulture);
        }
    }

    void SaveData(float height, float weight)
    {
        PlayerPrefs.SetFloat(IMC_HEIGHT, height);
        PlayerPrefs.SetFloat(IMC_WEIGHT, weight);
    }

}
