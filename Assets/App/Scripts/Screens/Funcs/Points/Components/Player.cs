﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    [SerializeField] private int id;
    [SerializeField] private RectTransform rectT;
    [SerializeField] private Image img_bg_main;
    [SerializeField] private Button btn_changeColor;
    [SerializeField] private List<Color32> list_color;
    [SerializeField] private Button btn_close;
    public TMP_InputField inputF_name;
    public ExtraComponents.FieldMoreLess fml;
    public Button btn_add;
    public int current_color = 0;

    void Awake()
    {
        #region Set References
        rectT = GetComponent<RectTransform>();

        img_bg_main = rectT.GetChild(0).GetChild(0).GetComponent<Image>();
        
        btn_changeColor = rectT.GetChild(1).GetComponent<Button>();
        //Cores adicionadas no Inspector

        btn_close = rectT.GetChild(2).GetComponent<Button>();

        inputF_name = rectT.GetChild(3).GetChild(1).GetComponent<TMP_InputField>();

        fml = new ExtraComponents.FieldMoreLess(
            rectT.GetChild(4).GetChild(1).GetComponent<TMP_InputField>(),
            0
        );

        btn_add = rectT.GetChild(5).GetComponent<Button>();
        #endregion

        btn_changeColor.onClick.AddListener(ChangeColor);
        btn_close.onClick.AddListener(RemovePlayer);
        btn_add.onClick.AddListener(OpenPlayer);
    }

    void ChangeColor(){
        if(list_color.ToArray().Length-1 == current_color)
            current_color = 0;
        else
            current_color++;

        SetColor(current_color);
    }

    void SetColor(int new_color){
        img_bg_main.color = list_color[new_color];
    }

    public void SetItems(string name, int point, int playerColor){
        inputF_name.text = name;
        fml.inputF.text = point.ToString();
        current_color = playerColor;

        SetColor(current_color);
        
        Destroy(btn_add.gameObject);
    }

    void OpenPlayer()
    {
        Destroy(btn_add.gameObject);

        ScreenManager.Instance.Points.AddNewPlayer();
    }

    public void ModifyId(int new_id){
        id = new_id;
        AdjustPlayerPosition();
    }

    void AdjustPlayerPosition()
    {
        int playerPosx;

        if (id % 2 == 0)
            playerPosx = 0;
        else
            playerPosx = 475;

        rectT.anchoredPosition = new Vector2(playerPosx, -(int)(id/2) * ScreenManager.Instance.Points.playerDistanceY);
    }

    void RemovePlayer()
    {
        ScreenManager.Instance.Points.RemovePlayerFromList(id);

        Destroy(rectT.gameObject);
    }

}
