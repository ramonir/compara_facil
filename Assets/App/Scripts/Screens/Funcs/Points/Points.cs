﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Points : MonoBehaviour
{
    [System.Serializable]
    public class PlayerItems
    {
        public string name;
        public int point;
        public int playerColor;

        public PlayerItems(string new_name, int new_amount, int new_color)
        {
            name = new_name;
            point = new_amount;
            playerColor = new_color;
        }
    }

    [Header("Panel Top")]
    [SerializeField] private Button btn_save;
    [SerializeField] private Button btn_reset;
    private ScrollRect scroll_players;
    private RectTransform point_player;
    private RectTransform prefab_player;
    [SerializeField] private List<Player> list_player;

    [Header("Infos")]
    public int playerDistanceY = 550;

    const string NEW_PLAYER_PLAYERS = "NEW_PLAYER_PLAYERS";
    // const string NEW_PLAYER_UNIT_POINT = "NEW_PLAYER_UNIT_POINT";

    void Start()
    {
        ScreenManager.Instance.Points = this;

        btn_reset.onClick.AddListener(ResetData);
        btn_save.onClick.AddListener(SaveData);

        scroll_players = GetComponentInChildren<ScrollRect>();
        point_player = scroll_players.content.GetChild(0).GetComponent<RectTransform>();

        prefab_player = (Resources.Load("Prefabs/Screens/Points/Components/player") as GameObject).GetComponent<RectTransform>();

        RequestData();
    }

    void RequestData()
    {
        if (PlayerPrefs.HasKey(NEW_PLAYER_PLAYERS))
        {
            PlayerItems[] arr_playerItems;
            arr_playerItems = JsonHelper.FromJson<PlayerItems>(PlayerPrefs.GetString(NEW_PLAYER_PLAYERS));
            AddNewPlayers(arr_playerItems);
        }
        else
            DefaultPlayerList();

    }

    void DefaultPlayerList()
    {
        AddNewPlayer();
        list_player[0].btn_add.onClick.Invoke();
    }

    public void AddNewPlayer(string name = null, int point = 0, int playerColor = 0)
    {
        var gO_player = Instantiate(prefab_player, point_player.transform.position, point_player.transform.rotation, point_player);
        var player = gO_player.GetComponent<Player>();

        list_player.Add(player);

        player.ModifyId(list_player.ToArray().Length - 1);
        AdjustScrollSize();

        if (name != null) player.SetItems(name, point, playerColor);
    }

    public void AddNewPlayers(PlayerItems[] arr_item)
    {
        for (int i = 0; i < arr_item.Length; i++)
            AddNewPlayer(arr_item[i].name, arr_item[i].point, arr_item[i].playerColor);

        AddNewPlayer();
    }

    void AdjustScrollSize()
    {
        var scrollExtraSizeY = 25;
        int scrollSizeY = ((int)((1+list_player.ToArray().Length)/2) * playerDistanceY) + scrollExtraSizeY;

        scroll_players.content.sizeDelta = new Vector2(scroll_players.content.sizeDelta.x, scrollSizeY);
    }

    public void RemovePlayerFromList(int id)
    {
        list_player.RemoveAt(id);

        ModifyPlayerVariableValues();
        AdjustScrollSize();
    }

    void ModifyPlayerVariableValues()
    {
        for (int i = 0; i < list_player.ToArray().Length; i++)
        {
            list_player[i].ModifyId(i);
        }
    }

    void ResetData(){
        PlayerPrefs.DeleteKey(NEW_PLAYER_PLAYERS);

        list_player.Clear();
        for (int i = 0; i < point_player.childCount; i++)
        {
            Destroy(point_player.GetChild(i).gameObject);
        }

        AddNewPlayer();
    }

    void SaveData()
    {
        List<PlayerItems> items = new List<PlayerItems>();
        for (int i = 0; i < list_player.ToArray().Length - 1; i++)
        {
            string name = list_player[i].inputF_name.text;
            int amount = int.Parse(list_player[i].fml.inputF.text);
            int playerColor = list_player[i].current_color;

            items.Add(new PlayerItems(name, amount, playerColor));
        }

        string json_players = JsonHelper.ToJson<PlayerItems>(items);
        PlayerPrefs.SetString(NEW_PLAYER_PLAYERS, json_players);

        // PlayerPrefs.SetInt(NEW_PLAYER_UNIT_POINT, unitPoint);
    }

}
