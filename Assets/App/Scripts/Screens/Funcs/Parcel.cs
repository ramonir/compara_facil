﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static TMPro.TMP_Dropdown;

public class Parcel : MonoBehaviour
{
    [System.Serializable]
    public class Field
    {
        public TMP_InputField inputF;
        public Button btn_lock;
        public TextMeshProUGUI txt_warning;
        public bool openned;
    }

    [System.Serializable]
    public class IncomeItems
    {
        public string name;
        public float value;
    }

    [Header("Panel Top")]
    [SerializeField] private Field field_totalValue;
    [SerializeField] private Field field_aVista;
    [SerializeField] private Field field_parcelValue;
    [SerializeField] private Field field_discount;

    [SerializeField] private ExtraComponents.FieldMoreLess fml_parcels;
    private IncomeItems[] arr_incomeItems;
    [SerializeField] private TMP_Dropdown dd_income;
    [SerializeField] private TMP_InputField if_income;

    [SerializeField] private Button btn_calc;

    [Header("Prefabs")]
    private Image img_aVista;
    private Image img_aPrazo;
    private Image img_ambos;

    [Header("Panel Bot")]
    [SerializeField] private RectTransform panel_bot;
    [SerializeField] private RectTransform point_img;
    [SerializeField] private TextMeshProUGUI txt_resultAVista;
    [SerializeField] private TextMeshProUGUI txt_resultAPrazo;

    [Header("Sprites")]
    [SerializeField] private Sprite sprite_lock;
    [SerializeField] private Sprite sprite_unlock;

    const string PARCEL_TOTAL_VALUE = "PARCEL_TOTAL_VALUE";
    const string PARCEL_A_VISTA = "PARCEL_A_VISTA";
    const string PARCEL_PARCELS = "PARCEL_PARCELS";
    const string PARCEL_DROPDOWN_INCOME = "PARCEL_DROPDOWN_INCOME";
    const string PARCEL_INCOME_VALUE = "PARCEL_INCOME_VALUE";

    void Start()
    {
        field_totalValue.inputF.onEndEdit.AddListener(delegate
        {
            UtilityHelper.AdjustInputFieldToReal(field_totalValue.inputF);
            CompleteParcelValue();
            CompleteDiscount();
        });
        field_parcelValue.inputF.onEndEdit.AddListener(delegate
        {
            UtilityHelper.AdjustInputFieldToReal(field_parcelValue.inputF);
            CompleteTotalValue();
        });
        field_aVista.inputF.onEndEdit.AddListener(delegate
        {
            UtilityHelper.AdjustInputFieldToReal(field_aVista.inputF);
            CompleteDiscount();
        });
        field_discount.inputF.onEndEdit.AddListener(delegate { CompleteAVista(); });

        Field[] field_left = { field_totalValue, field_parcelValue };
        field_totalValue.btn_lock.onClick.AddListener(() => LockField(field_left));
        field_parcelValue.btn_lock.onClick = field_totalValue.btn_lock.onClick;
        Field[] field_right = { field_aVista, field_discount };
        field_aVista.btn_lock.onClick.AddListener(() => LockField(field_right));
        field_discount.btn_lock.onClick = field_aVista.btn_lock.onClick;

        fml_parcels = new ExtraComponents.FieldMoreLess(fml_parcels.inputF, 0);
        fml_parcels.inputF.onValueChanged.AddListener(ParcelsOnValueChanged);
        CreateDropDownList(dd_income);
        dd_income.onValueChanged.AddListener(CheckDropDown);

        btn_calc.onClick.AddListener(Calc);

        img_aVista = (Resources.Load("Prefabs/Screens/Parcel/Components/img_aVista") as GameObject).GetComponent<Image>();
        img_aPrazo = (Resources.Load("Prefabs/Screens/Parcel/Components/img_aPrazo") as GameObject).GetComponent<Image>();
        img_ambos = (Resources.Load("Prefabs/Screens/GenericComponents/img_ambos") as GameObject).GetComponent<Image>();

        RequestData();
    }
    void LockField(Field[] fields)
    {
        foreach (Field field in fields)
        {
            field.openned = !field.openned;

            field.inputF.image.fillCenter = !field.openned;
            field.inputF.interactable = field.openned;

            if (field.openned) field.btn_lock.image.sprite = sprite_unlock;
            else field.btn_lock.image.sprite = sprite_lock;
        }
    }

    void CreateDropDownList(TMP_Dropdown dd)
    {
        // dd.options.Add(new OptionData("Poupança (2,62%)"));
        // dd.options.Add(new OptionData("Tesouro Selic (3,75%)"));
        // dd.options.Add(new OptionData("NuBank (3,65%)"));
        // dd.options.Add(new OptionData("Nenhum (0%)"));
        // dd.options.Add(new OptionData("Personalizado"));

        string json = (Resources.Load("Json/json_income") as Object).ToString();
        arr_incomeItems = JsonHelper.FromJson<IncomeItems>(json);

        for (int i = 0; i < arr_incomeItems.Length; i++)
            dd.options.Add(new OptionData($"{arr_incomeItems[i].name} ({arr_incomeItems[i].value}%)"));

        dd.options.Add(new OptionData("Personalizado"));
    }

    void CheckDropDown(int value)
    {
        if (value == 4) if_income.gameObject.SetActive(true);
        else if_income.gameObject.SetActive(false);
    }

    void CompleteParcelValue()
    {
        if (field_totalValue.inputF.text == "") return;
        float totalValue = UtilityHelper.AdjustStringRealToFloat(field_totalValue.inputF.text);
        int parcels = int.Parse(fml_parcels.inputF.text);
        float parcelValue = totalValue / parcels;
        if (parcels <= 0) parcelValue = 0;
        field_parcelValue.inputF.text = parcelValue.ToString();
        UtilityHelper.AdjustInputFieldToReal(field_parcelValue.inputF);
    }

    void CompleteTotalValue()
    {
        if (field_parcelValue.inputF.text == "") return;
        float parcelValue = UtilityHelper.AdjustStringRealToFloat(field_parcelValue.inputF.text);
        int parcels = int.Parse(fml_parcels.inputF.text);
        float totalValue = parcelValue * parcels;
        if (parcels <= 0) totalValue = 0;
        field_totalValue.inputF.text = totalValue.ToString();
        UtilityHelper.AdjustInputFieldToReal(field_totalValue.inputF);
    }

    void CompleteAVista()
    {
        if (field_totalValue.inputF.text == "") return;
        if (field_discount.inputF.text == "") return;
        float totalValue = UtilityHelper.AdjustStringRealToFloat(field_totalValue.inputF.text);
        float discount = UtilityHelper.AdjustStringRealToFloat(field_discount.inputF.text);
        float aVista = totalValue - (totalValue * (discount / 100));
        field_aVista.inputF.text = aVista.ToString();
        UtilityHelper.AdjustInputFieldToReal(field_aVista.inputF);
    }

    void CompleteDiscount()
    {
        if (field_totalValue.inputF.text == "") return;
        if (field_aVista.inputF.text == "") return;
        float totalValue = UtilityHelper.AdjustStringRealToFloat(field_totalValue.inputF.text);
        float aVista = UtilityHelper.AdjustStringRealToFloat(field_aVista.inputF.text);
        float discount = (totalValue - aVista) / (totalValue / 100);
        field_discount.inputF.text = discount.ToString("00.0");
    }

    void ParcelsOnValueChanged(string nothing)
    {
        if (field_totalValue.openned) CompleteParcelValue();
        else CompleteTotalValue();
    }

    void RequestData()
    {
        if (PlayerPrefs.HasKey(PARCEL_TOTAL_VALUE)) field_totalValue.inputF.text = PlayerPrefs.GetFloat(PARCEL_TOTAL_VALUE).ToString();
        UtilityHelper.AdjustInputFieldToReal(field_totalValue.inputF);
        CompleteParcelValue();

        if (PlayerPrefs.HasKey(PARCEL_A_VISTA)) field_aVista.inputF.text = PlayerPrefs.GetFloat(PARCEL_A_VISTA).ToString();
        UtilityHelper.AdjustInputFieldToReal(field_aVista.inputF);
        CompleteDiscount();

        if (PlayerPrefs.HasKey(PARCEL_PARCELS)) fml_parcels.inputF.text = PlayerPrefs.GetInt(PARCEL_PARCELS).ToString();

        if (PlayerPrefs.HasKey(PARCEL_DROPDOWN_INCOME)) dd_income.value = PlayerPrefs.GetInt(PARCEL_DROPDOWN_INCOME);
        if (dd_income.value == dd_income.options.ToArray().Length - 1) if_income.text = PlayerPrefs.GetFloat(PARCEL_INCOME_VALUE).ToString();
    }

    void Calc()
    {
        #region Validation
        Field[] fields = { field_totalValue, field_parcelValue, field_aVista, field_discount };
        foreach (Field field in fields)
        {
            if (!UtilityHelper.ValidateInputField(field.inputF, field.txt_warning)) return;
        }
        #endregion //Validation

        var value_totalValue = UtilityHelper.AdjustStringRealToFloat(field_totalValue.inputF.text);
        var value_parcelValue = UtilityHelper.AdjustStringRealToFloat(field_parcelValue.inputF.text);
        var value_aVista = UtilityHelper.AdjustStringRealToFloat(field_aVista.inputF.text);
        var value_discount = UtilityHelper.AdjustStringRealToFloat(field_discount.inputF.text);
        int value_parcels = int.Parse(fml_parcels.inputF.text);
        var income = GetDropDownIncome(dd_income);
        print(income);

        float lucroMensalPorcentagem = (income / 12) / 100;

        #region aVista Calc
        float aVistaGain = (value_totalValue - value_aVista);
        for (int i = 0; i < value_parcels; i++)
        {
            var lucroDesseMes = aVistaGain * lucroMensalPorcentagem;
            aVistaGain += lucroDesseMes;
        }
        #endregion //aVista Calc
        string text_aVistaGain = UtilityHelper.AdjustStringToReal(aVistaGain.ToString());
        txt_resultAVista.text = $"R$ {text_aVistaGain}";

        #region parceled Calc
        float parceledGain = value_totalValue;
        for (int i = 0; i < value_parcels; i++)
        {
            var lucroDesseMes = parceledGain * lucroMensalPorcentagem;
            parceledGain += lucroDesseMes;
            parceledGain -= value_parcelValue;
        }
        #endregion
        string text_parceledGain = UtilityHelper.AdjustStringToReal(parceledGain.ToString());
        txt_resultAPrazo.text = $"R$ {text_parceledGain}";

        panel_bot.gameObject.SetActive(true);
        foreach (RectTransform child in point_img)
        {
            GameObject.Destroy(child.gameObject);
        }

        Image img;
        if (parceledGain < aVistaGain) img = img_aVista;
        else if (parceledGain > aVistaGain) img = img_aPrazo;
        else img = img_ambos;
        Instantiate(img, point_img.transform.position, point_img.transform.rotation, point_img);

        SaveData(value_totalValue, value_aVista, value_parcels, dd_income.value, income);
    }

    float GetDropDownIncome(TMP_Dropdown dd)
    {
        float value;
        if (dd.value == dd.options.ToArray().Length - 1)
        {
            if (if_income.text == "") return 0;
            value = UtilityHelper.AdjustStringRealToFloat(if_income.text);
            return value;
        }
        else
        {
            value = arr_incomeItems[dd_income.value].value;
            return value;
        }

        // if (dd.value == 0) return 2.97f;
        // else if (dd.value == 1) return 4.25f;
        // else if (dd.value == 2) return 4.15f;
        // else if (dd.value == 3) return 0f;
        // else
        // {
        //     if (if_income.text == "") return 0;
        //     float value = UtilityHelper.AdjustStringRealToFloat(if_income.text);
        //     return value;
        // }
    }

    void SaveData(float totalValue, float aVista, int parcels, int income, float income_value)
    {
        PlayerPrefs.SetFloat(PARCEL_TOTAL_VALUE, totalValue);
        PlayerPrefs.SetFloat(PARCEL_A_VISTA, aVista);
        PlayerPrefs.SetInt(PARCEL_PARCELS, parcels);
        PlayerPrefs.SetInt(PARCEL_DROPDOWN_INCOME, income);
        if (income == 4) PlayerPrefs.SetFloat(PARCEL_INCOME_VALUE, income_value);
        else PlayerPrefs.SetFloat(PARCEL_INCOME_VALUE, 0);
    }
}
