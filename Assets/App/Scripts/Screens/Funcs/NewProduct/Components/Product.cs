﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Product : MonoBehaviour
{
    [SerializeField] private int id;
    [SerializeField] private RectTransform rectT;
    [SerializeField] private Button btn_close;
    public TMP_InputField inputF_name;
    public TMP_InputField inputF_value;
    public ExtraComponents.FieldMoreLess fml;
    public Button btn_add;

    void Awake()
    {
        #region Set References
        rectT = GetComponent<RectTransform>();

        btn_close = rectT.GetChild(1).GetComponent<Button>();

        inputF_name = rectT.GetChild(2).GetChild(1).GetComponent<TMP_InputField>();
        inputF_value = rectT.GetChild(3).GetChild(1).GetComponent<TMP_InputField>();

        fml = new ExtraComponents.FieldMoreLess(
            rectT.GetChild(4).GetChild(1).GetComponent<TMP_InputField>(),
            0
        );

        btn_add = rectT.GetChild(5).GetComponent<Button>();
        #endregion

        inputF_value.onEndEdit.AddListener(delegate { UtilityHelper.AdjustInputFieldToReal(inputF_value); });

        btn_close.onClick.AddListener(RemoveProduct);
        btn_add.onClick.AddListener(OpenProduct);
    }

    public void SetItems(string name, float value, int amount){
        inputF_name.text = name;
        inputF_value.text = value.ToString();
        UtilityHelper.AdjustInputFieldToReal(inputF_value);
        fml.inputF.text = amount.ToString();

        Destroy(btn_add.gameObject);
    }

    void OpenProduct()
    {
        Destroy(btn_add.gameObject);

        ScreenManager.Instance.NewProduct.AddNewProduct();
    }

    public void ModifyId(int new_id){
        id = new_id;
        AdjustProductPosition();
    }

    void AdjustProductPosition()
    {
        rectT.anchoredPosition = new Vector2(id * ScreenManager.Instance.NewProduct.productDistance, 0); // Change Position
    }

    void RemoveProduct()
    {
        ScreenManager.Instance.NewProduct.RemoveProductFromList(id);

        Destroy(rectT.gameObject);
    }

}
