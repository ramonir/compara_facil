﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewProduct : MonoBehaviour
{
    [System.Serializable]
    public class ProductItems
    {
        public string name;
        public float value;
        public int amount;

        public ProductItems(string new_name, float new_value, int new_amount)
        {
            name = new_name;
            value = new_value;
            amount = new_amount;
        }
    }

    [Header("Panel Top")]
    private ScrollRect scroll_products;
    private RectTransform point_product;
    private RectTransform prefab_product;
    [SerializeField] private List<Product> list_product;
    [SerializeField] private TextMeshProUGUI txt_productWarning;
    [SerializeField] private ExtraComponents.RequiredField reqF_unitValue;
    [SerializeField] private ExtraComponents.FieldMoreLess fml_unitAmount;
    [SerializeField] private Button btn_calc;

    [Header("Panel Bot")]
    [SerializeField] private RectTransform panel_bot;
    [SerializeField] private TextMeshProUGUI txt_resultGain;
    [SerializeField] private TextMeshProUGUI txt_resultGainDesc;
    [SerializeField] private TextMeshProUGUI txt_resultRealGain;
    [SerializeField] private TextMeshProUGUI txt_resultRealGainDesc;

    [Header("Infos")]
    public int productDistance = 425;

    const string NEW_PRODUCT_PRODUCTS = "NEW_PRODUCT_PRODUCTS";
    const string NEW_PRODUCT_UNIT_VALUE = "NEW_PRODUCT_UNIT_VALUE";
    const string NEW_PRODUCT_UNIT_AMOUNT = "NEW_PRODUCT_UNIT_AMOUNT";

    void Start()
    {
        ScreenManager.Instance.NewProduct = this;

        scroll_products = GetComponentInChildren<ScrollRect>();
        point_product = scroll_products.content.GetChild(0).GetComponent<RectTransform>();

        prefab_product = (Resources.Load("Prefabs/Screens/NewProduct/Components/product") as GameObject).GetComponent<RectTransform>();

        reqF_unitValue.inputF.onEndEdit.AddListener(delegate { UtilityHelper.AdjustInputFieldToReal(reqF_unitValue.inputF); });
        fml_unitAmount = new ExtraComponents.FieldMoreLess(fml_unitAmount.inputF, 0);
        btn_calc.onClick.AddListener(Calc);

        RequestData();
    }

    void RequestData()
    {
        if (PlayerPrefs.HasKey(NEW_PRODUCT_PRODUCTS))
        {
            ProductItems[] arr_productItems;
            arr_productItems = JsonHelper.FromJson<ProductItems>(PlayerPrefs.GetString(NEW_PRODUCT_PRODUCTS));
            AddNewProducts(arr_productItems);
        }
        else
            DefaultProductList();

        if (PlayerPrefs.HasKey(NEW_PRODUCT_UNIT_VALUE)) reqF_unitValue.inputF.text = PlayerPrefs.GetFloat(NEW_PRODUCT_UNIT_VALUE).ToString();
        UtilityHelper.AdjustInputFieldToReal(reqF_unitValue.inputF);

        if (PlayerPrefs.HasKey(NEW_PRODUCT_UNIT_AMOUNT)) fml_unitAmount.inputF.text = PlayerPrefs.GetInt(NEW_PRODUCT_UNIT_AMOUNT).ToString();
    }

    void DefaultProductList()
    {
        AddNewProduct();
        list_product[0].btn_add.onClick.Invoke();
    }

    public void AddNewProduct(string name = null, float value = 0, int amount = 0)
    {
        var gO_product = Instantiate(prefab_product, point_product.transform.position, point_product.transform.rotation, point_product);
        var product = gO_product.GetComponent<Product>();

        list_product.Add(product);

        product.ModifyId(list_product.ToArray().Length - 1);
        AdjustScrollSize();

        if (name != null) product.SetItems(name, value, amount);
    }

    public void AddNewProducts(ProductItems[] arr_item)
    {
        for (int i = 0; i < arr_item.Length; i++)
            AddNewProduct(arr_item[i].name, arr_item[i].value, arr_item[i].amount);

        AddNewProduct();
    }

    void AdjustScrollSize()
    {
        var scrollExtraSize = 25;

        scroll_products.content.sizeDelta = new Vector2((list_product.ToArray().Length * productDistance) + scrollExtraSize, scroll_products.content.sizeDelta.y);
    }

    public void RemoveProductFromList(int id)
    {
        list_product.RemoveAt(id);

        ModifyProductVariableValues();
        AdjustScrollSize();
    }

    void ModifyProductVariableValues()
    {
        for (int i = 0; i < list_product.ToArray().Length; i++)
        {
            list_product[i].ModifyId(i);
        }
    }

    void Calc()
    {
        for (int i = 0; i < list_product.ToArray().Length - 1; i++)
        {
            if (list_product[i].inputF_value.text == "")
            {
                if(!UtilityHelper.ValidateInputField(list_product[i].inputF_value, txt_productWarning)) return;
            }
        }
        if(!UtilityHelper.ValidateInputField(reqF_unitValue.inputF, reqF_unitValue.txt_warning)) return;

        panel_bot.gameObject.SetActive(true);

        float value_productsLoss = 0f;
        for (int i = 0; i < list_product.ToArray().Length - 1; i++)
        {
            var value_product = UtilityHelper.AdjustStringRealToFloat(list_product[i].inputF_value.text);
            int amount_product = int.Parse(list_product[i].fml.inputF.text);

            var value_totalProduct = value_product * amount_product;
            value_productsLoss += value_totalProduct;
        }
        var value_unit = UtilityHelper.AdjustStringRealToFloat(reqF_unitValue.inputF.text);
        int amount_unit = int.Parse(fml_unitAmount.inputF.text);

        var value_totalUnit = value_unit * amount_unit;

        var realGain = value_totalUnit - value_productsLoss;
        var gain = (realGain / value_totalUnit) * 100;
        Color color;
        if (realGain < 0)
        {
            ColorUtility.TryParseHtmlString("#C40000", out color); //Red

            txt_resultGainDesc.text = "de prejuízo";
            txt_resultRealGainDesc.text = "de prejuízo em reais";
            realGain *= -1;
            gain *= -1;
        }
        else
        {
            if (realGain == 0) ColorUtility.TryParseHtmlString("#0078FF", out color); //Blue
            else ColorUtility.TryParseHtmlString("#00B400", out color); //Green

            txt_resultGainDesc.text = "de lucro";
            txt_resultRealGainDesc.text = "de lucro em reais";
        }
        txt_resultGain.color = color;
        txt_resultRealGain.color = color;

        string text_realGain = UtilityHelper.AdjustStringToReal(realGain.ToString());
        txt_resultRealGain.text = $"R$ {text_realGain}";
        txt_resultGain.text = $"{gain.ToString("00.0")}%";

        SaveData(value_unit, amount_unit);
    }

    void SaveData(float unitValue, int unitAmount)
    {
        List<ProductItems> items = new List<ProductItems>();
        for (int i = 0; i < list_product.ToArray().Length - 1; i++)
        {
            string name = list_product[i].inputF_name.text;
            float value = UtilityHelper.AdjustStringRealToFloat(list_product[i].inputF_value.text);
            int amount = int.Parse(list_product[i].fml.inputF.text);

            items.Add(new ProductItems(name, value, amount));
        }

        string json_products = JsonHelper.ToJson<ProductItems>(items);
        PlayerPrefs.SetString(NEW_PRODUCT_PRODUCTS, json_products);

        PlayerPrefs.SetFloat(NEW_PRODUCT_UNIT_VALUE, unitValue);
        PlayerPrefs.SetInt(NEW_PRODUCT_UNIT_AMOUNT, unitAmount);
    }

}
