﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System.Globalization;
using App.Core;

public class Fuel : MonoBehaviour
{
    [Header("Panel Top")]
    [SerializeField] private ExtraComponents.RequiredField reqF_etanol;
    [SerializeField] private ExtraComponents.RequiredField reqF_gasolina;

    [SerializeField] private Button btn_calc;

    [Header("Prefabs")]
    private Image img_etanol;
    private Image img_gasolina;
    private Image img_ambos;

    [Header("Panel Bot")]
    [SerializeField] private RectTransform panel_bot;
    [SerializeField] private RectTransform point_img;
    [SerializeField] private TextMeshProUGUI txt_result;

    const string FUEL_ETANOL = "FUEL_ETANOL";
    const string FUEL_GASOLINA = "FUEL_GASOLINA";

    void Start()
    {
        reqF_etanol.inputF.onEndEdit.AddListener(delegate { UtilityHelper.AdjustInputFieldToReal(reqF_etanol.inputF); });
        reqF_gasolina.inputF.onEndEdit.AddListener(delegate { UtilityHelper.AdjustInputFieldToReal(reqF_gasolina.inputF); });
        btn_calc.onClick.AddListener(Calc);
        img_etanol = (Resources.Load("Prefabs/Screens/Fuel/Components/img_etanol") as GameObject).GetComponent<Image>();
        img_gasolina = (Resources.Load("Prefabs/Screens/Fuel/Components/img_gasolina") as GameObject).GetComponent<Image>();
        img_ambos = (Resources.Load("Prefabs/Screens/GenericComponents/img_ambos") as GameObject).GetComponent<Image>();

        RequestData();
    }

    void RequestData()
    {
        if (PlayerPrefs.HasKey(FUEL_ETANOL)) reqF_etanol.inputF.text = PlayerPrefs.GetFloat(FUEL_ETANOL).ToString();
        UtilityHelper.AdjustInputFieldToReal(reqF_etanol.inputF);
        if (PlayerPrefs.HasKey(FUEL_GASOLINA)) reqF_gasolina.inputF.text = PlayerPrefs.GetFloat(FUEL_GASOLINA).ToString();
        UtilityHelper.AdjustInputFieldToReal(reqF_gasolina.inputF);
    }

    void Calc()
    {
        if(!UtilityHelper.ValidateInputField(reqF_etanol.inputF, reqF_etanol.txt_warning)) return;
        if(!UtilityHelper.ValidateInputField(reqF_gasolina.inputF, reqF_gasolina.txt_warning)) return;

        var value_etanol = UtilityHelper.AdjustStringRealToFloat(reqF_etanol.inputF.text);
        var value_gasolina = UtilityHelper.AdjustStringRealToFloat(reqF_gasolina.inputF.text);

        var value_final = (value_etanol / value_gasolina) * 100;
        txt_result.text = $"{value_final.ToString("00.0")}%";

        panel_bot.gameObject.SetActive(true);
        foreach (RectTransform child in point_img)
        {
            GameObject.Destroy(child.gameObject);
        }

        Image img;
        if (value_final < 70f) img = img_etanol;
        else if (value_final > 75f) img = img_gasolina;
        else img = img_ambos;
        Instantiate(img, point_img.transform.position, point_img.transform.rotation, point_img);

        SaveData(value_etanol, value_gasolina);
    }

    void SaveData(float etanol, float gasolina)
    {
        PlayerPrefs.SetFloat(FUEL_ETANOL, etanol);
        PlayerPrefs.SetFloat(FUEL_GASOLINA, gasolina);
    }
}
