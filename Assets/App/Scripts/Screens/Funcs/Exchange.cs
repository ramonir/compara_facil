﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using App.Core;

public class Exchange : MonoBehaviour
{
    [Header("Panel Top")]
    [SerializeField] private ExtraComponents.RequiredField reqF_totalValue;
    [SerializeField] private ExtraComponents.RequiredField reqF_toExchangeValue;
    [SerializeField] private Button btn_calc;

    [Header("Panel Bot")]
    [SerializeField] private RectTransform panel_bot;
    private ScrollRect scroll_exchange;
    [SerializeField] private RectTransform point_ref;
    [SerializeField] private TextMeshProUGUI txt_resultValue;
    [SerializeField] private TextMeshProUGUI txt_resultTxt;
    [SerializeField] private TextMeshProUGUI txt_resultDesc;
    [SerializeField] private TextMeshProUGUI txt_notice;

    [Header("Prefabs")]
    private RectTransform ref_exchange;

    [Header("Sprites")]
    [SerializeField] private Sprite sprite_real_2;
    [SerializeField] private Sprite sprite_real_5;
    [SerializeField] private Sprite sprite_real_10;
    [SerializeField] private Sprite sprite_real_20;
    [SerializeField] private Sprite sprite_real_50;
    [SerializeField] private Sprite sprite_real_100;
    [SerializeField] private Sprite sprite_real_1;
    [SerializeField] private Sprite sprite_cent_5;
    [SerializeField] private Sprite sprite_cent_10;
    [SerializeField] private Sprite sprite_cent_25;
    [SerializeField] private Sprite sprite_cent_50;

    const string EXCHANGE_TOTAL_VALUE = "EXCHANGE_TOTAL_VALUE";
    const string EXCHANGE_TO_EXCHANGE_VALUE = "EXCHANGE_TO_EXCHANGE_VALUE";

    void Start()
    {
        reqF_totalValue.inputF.onEndEdit.AddListener(delegate { UtilityHelper.AdjustInputFieldToReal(reqF_totalValue.inputF); });
        reqF_toExchangeValue.inputF.onEndEdit.AddListener(delegate { UtilityHelper.AdjustInputFieldToReal(reqF_toExchangeValue.inputF); });
        btn_calc.onClick.AddListener(Calc);

        scroll_exchange = panel_bot.GetComponentInChildren<ScrollRect>();

        ref_exchange = (Resources.Load("Prefabs/Screens/Exchange/Components/refExchange") as GameObject).GetComponent<RectTransform>();

        RequestData();
    }

    void RequestData(){
        if(PlayerPrefs.HasKey(EXCHANGE_TOTAL_VALUE)) reqF_totalValue.inputF.text = PlayerPrefs.GetFloat(EXCHANGE_TOTAL_VALUE).ToString();
        UtilityHelper.AdjustInputFieldToReal(reqF_totalValue.inputF);
        if(PlayerPrefs.HasKey(EXCHANGE_TO_EXCHANGE_VALUE)) reqF_toExchangeValue.inputF.text = PlayerPrefs.GetFloat(EXCHANGE_TO_EXCHANGE_VALUE).ToString();
        UtilityHelper.AdjustInputFieldToReal(reqF_toExchangeValue.inputF);
    }

    void Calc()
    {
        if(!UtilityHelper.ValidateInputField(reqF_totalValue.inputF, reqF_totalValue.txt_warning)) return;
        if(!UtilityHelper.ValidateInputField(reqF_toExchangeValue.inputF, reqF_toExchangeValue.txt_warning)) return;

        panel_bot.gameObject.SetActive(true);

        var value_totalValue = UtilityHelper.AdjustStringRealToFloat(reqF_totalValue.inputF.text);
        var value_toExchangeValue = UtilityHelper.AdjustStringRealToFloat(reqF_toExchangeValue.inputF.text);

        var exchangeValue = value_toExchangeValue - value_totalValue;
        Color color;
        if (exchangeValue < 0)
        {
            ColorUtility.TryParseHtmlString("#C40000", out color); //Red
            
            txt_resultTxt.text = "faltando";
            txt_resultDesc.text = "A melhor opção para\ncompletar nesse caso..."; //substituir por const?
            exchangeValue *= -1;
        }
        else if (exchangeValue == 0)
        {
            ColorUtility.TryParseHtmlString("#0078FF", out color); //Blue
            
            txt_resultTxt.text = "de troco";
            txt_resultDesc.text = "Não precisa de troco";
        }
        else
        {
            ColorUtility.TryParseHtmlString("#00B400", out color); //Green

            txt_resultTxt.text = "de troco";
            txt_resultDesc.text = "A melhor opção de\ntroco nesse caso...";
        }
            txt_resultValue.color = color;

        string text_exchangeValue = UtilityHelper.AdjustStringToReal(exchangeValue.ToString());
        txt_resultValue.text = $"R$ {text_exchangeValue}";

        foreach (RectTransform child in point_ref)
        {
            GameObject.Destroy(child.gameObject);
        }

        float retiredValue = 0f;
        int posCount = 0;
        int exchangeCount = 1;
        float[] array_exchangeFloatValue = { 100, 50, 20, 10, 5, 2, 1, 0.50f, 0.25f, 0.10f, 0.046f };
        for (float count = exchangeValue; count > 0.04f; count -= retiredValue * exchangeCount)
        {
            if (exchangeValue < 0.046f) break;
            for (int i = 0; i <= 10; i++)
            {
                if (exchangeValue >= array_exchangeFloatValue[i])
                {
                    retiredValue = array_exchangeFloatValue[i];
                    exchangeValue -= retiredValue;
                    exchangeCount = 1;
                    while (exchangeValue >= array_exchangeFloatValue[i])
                    {
                        exchangeValue -= retiredValue;
                        exchangeCount++;
                    }
                    SetExchangeProperties(retiredValue, i, exchangeCount, posCount);
                    posCount++;
                }
            }
        }
        if (exchangeValue <= 0.009f) txt_notice.enabled = false;
        else txt_notice.enabled = true;
        text_exchangeValue = UtilityHelper.AdjustStringToReal(exchangeValue.ToString());
        txt_notice.text = $"*foram dispensados {text_exchangeValue} centavos";

        SaveData(value_totalValue, value_toExchangeValue);
    }

    void SetExchangeProperties(float thisExchangeValue, int exchangePos, int exchangeCount, int posCount)
    {
        var gO = Instantiate(ref_exchange, point_ref.transform.position, point_ref.transform.rotation, point_ref);
        var refDistance = 250;

        gO.anchoredPosition -= new Vector2(0, posCount * refDistance); // Change Position
        scroll_exchange.content.sizeDelta = new Vector2(scroll_exchange.content.sizeDelta.x, (posCount + 1) * refDistance);

        Image img = gO.GetComponentInChildren<Image>();
        TextMeshProUGUI txt = gO.GetComponentInChildren<TextMeshProUGUI>();
        Sprite[] sprite = {sprite_real_100, sprite_real_50, sprite_real_20, sprite_real_10, sprite_real_5,
        sprite_real_2, sprite_real_1, sprite_cent_50, sprite_cent_25, sprite_cent_10, sprite_cent_5};
        int[] array_exchangeRealValue = { 100, 50, 20, 10, 5, 2, 1, 50, 25, 10, 5 };

        string exchangeTitle;
        string exchangeDesc;
        if (exchangePos <= 5)
        {
            if (exchangeCount == 1) exchangeTitle = "Cédula";
            else exchangeTitle = "Cédulas";
            exchangeDesc = "reais";
        }
        else
        {
            if (exchangeCount == 1) exchangeTitle = "Moeda";
            else exchangeTitle = "Moedas";
            if (exchangePos == 6) exchangeDesc = "real";
            else exchangeDesc = "centavos";
        }
        img.sprite = sprite[exchangePos];
        txt.text = $"<size=150%>{exchangeCount}</size> {exchangeTitle}\n<size=75%>de {array_exchangeRealValue[exchangePos]} {exchangeDesc}</size>";
    }

    void SaveData(float totalValue, float toExchangeValue){
        PlayerPrefs.SetFloat(EXCHANGE_TOTAL_VALUE, totalValue);
        PlayerPrefs.SetFloat(EXCHANGE_TO_EXCHANGE_VALUE, toExchangeValue);
    }
}
