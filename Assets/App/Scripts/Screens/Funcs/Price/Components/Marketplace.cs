﻿using System.Collections;
using System.Collections.Generic;
using App.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Marketplace : MonoBehaviour
{
    [SerializeField] private int id;
    [SerializeField] private RectTransform rectT;
    [SerializeField] private Button btn_close;
    public TMP_InputField inputF_name;
    public TMP_InputField inputF_commision;
    public TMP_InputField inputF_rate;
    public TMP_InputField inputF_profit;
    public TMP_InputField inputF_price;
    public Button btn_add;

    void Awake()
    {
        #region Set References
        rectT = GetComponent<RectTransform>();

        btn_close = rectT.GetChild(1).GetComponent<Button>();

        inputF_name = rectT.GetChild(2).GetChild(1).GetComponent<TMP_InputField>();
        inputF_commision = rectT.GetChild(3).GetChild(1).GetComponent<TMP_InputField>();
        inputF_rate = rectT.GetChild(4).GetChild(1).GetComponent<TMP_InputField>();
        inputF_profit = rectT.GetChild(5).GetChild(1).GetComponent<TMP_InputField>();
        inputF_price = rectT.GetChild(6).GetChild(1).GetComponent<TMP_InputField>();

        btn_add = rectT.GetChild(7).GetComponent<Button>();
        #endregion

        inputF_rate.onEndEdit.AddListener(delegate { UtilityHelper.AdjustInputFieldToReal(inputF_rate); });

        btn_close.onClick.AddListener(RemoveMarketplace);
        btn_add.onClick.AddListener(OpenMarketplace);
    }

    public void SetItems(string name, float comission, float rate){
        inputF_name.text = name;
        inputF_commision.text = comission.ToString();
        inputF_rate.text = rate.ToString();
        // inputF_profit.text = profit.ToString();
        // inputF_price.text = price.ToString();
        // UtilityHelper.AdjustInputFieldToReal(inputF_commision);

        Destroy(btn_add.gameObject);
    }

    void OpenMarketplace()
    {
        Destroy(btn_add.gameObject);

        ScreenManager.Instance.Price.AddMarketplace();
    }

    public void ModifyId(int new_id){
        id = new_id;
        AdjustMarketplacePosition();
    }

    void AdjustMarketplacePosition()
    {
        rectT.anchoredPosition = new Vector2(id * ScreenManager.Instance.Price.productDistance, 0); // Change Position
    }

    void RemoveMarketplace()
    {
        ScreenManager.Instance.Price.RemoveMarketplaceFromList(id);

        Destroy(rectT.gameObject);
    }

}
