﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Core;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Price : MonoBehaviour
{
    [System.Serializable]
    public class MarketplaceItems
    {
        public string name;
        public float commission;
        public float rate;

        public MarketplaceItems(string new_name, float new_commission, float new_rate)
        {
            name = new_name;
            commission = new_commission;
            rate = new_rate;
        }
    }

    [Header("Panel Top")]
    private ScrollRect scroll_marketplaces;
    private RectTransform point_marketplace;
    private RectTransform prefab_marketplace;
    [SerializeField] private List<Marketplace> list_marketplace;
    [SerializeField] private TextMeshProUGUI txt_marketplaceWarning;
    [SerializeField] private ExtraComponents.RequiredField reqF_value;
    [SerializeField] private ExtraComponents.RequiredField reqF_margin;
    // [SerializeField] private ExtraComponents.FieldMoreLess fml_unitAmount;
    [SerializeField] private Button btn_calc;

    [Header("Panel Bot")]
    // [SerializeField] private RectTransform panel_bot;
    // [SerializeField] private TextMeshProUGUI txt_resultGain;
    // [SerializeField] private TextMeshProUGUI txt_resultGainDesc;
    // [SerializeField] private TextMeshProUGUI txt_resultRealGain;
    // [SerializeField] private TextMeshProUGUI txt_resultRealGainDesc;

    [Header("Infos")]
    public int productDistance = 425;

    const string NEW_MARKETPLACE_MARKETPLACES = "NEW_MARKETPLACE_MARKETPLACES";
    const string NEW_MARKETPLACE_VALUE = "NEW_MARKETPLACE_VALUE";
    const string NEW_MARKETPLACE_MARGIN = "NEW_MARKETPLACE_MARGIN";

    void Start()
    {
        ScreenManager.Instance.Price = this;

        scroll_marketplaces = GetComponentInChildren<ScrollRect>();
        point_marketplace = scroll_marketplaces.content.GetChild(0).GetComponent<RectTransform>();

        prefab_marketplace = (Resources.Load("Prefabs/Screens/Price/Components/marketplace") as GameObject).GetComponent<RectTransform>();

        reqF_value.inputF.onEndEdit.AddListener(delegate { UtilityHelper.AdjustInputFieldToReal(reqF_value.inputF); });
        // fml_unitAmount = new ExtraComponents.FieldMoreLess(fml_unitAmount.inputF, 0);
        btn_calc.onClick.AddListener(Calc);

        RequestData();
    }

    void RequestData()
    {
        if (PlayerPrefs.HasKey(NEW_MARKETPLACE_MARKETPLACES))
        {
            MarketplaceItems[] arr_productItems;
            arr_productItems = JsonHelper.FromJson<MarketplaceItems>(PlayerPrefs.GetString(NEW_MARKETPLACE_MARKETPLACES));
            AddMarketplaces(arr_productItems);
        }
        else
            DefaultMarketplaceList();

        if (PlayerPrefs.HasKey(NEW_MARKETPLACE_VALUE)) reqF_value.inputF.text = PlayerPrefs.GetFloat(NEW_MARKETPLACE_VALUE).ToString();
        UtilityHelper.AdjustInputFieldToReal(reqF_value.inputF);

        if (PlayerPrefs.HasKey(NEW_MARKETPLACE_MARGIN)) reqF_margin.inputF.text = PlayerPrefs.GetFloat(NEW_MARKETPLACE_MARGIN).ToString();
        UtilityHelper.AdjustInputFieldToReal(reqF_margin.inputF);
    }

    void DefaultMarketplaceList()
    {
        AddMarketplace();
        list_marketplace[0].btn_add.onClick.Invoke();
    }

    public void AddMarketplace(string name = null, float comission = 5, float rate = 0)
    {
        var gO_product = Instantiate(prefab_marketplace, point_marketplace.transform.position, point_marketplace.transform.rotation, point_marketplace);
        var marketplace = gO_product.GetComponent<Marketplace>();

        list_marketplace.Add(marketplace);

        marketplace.ModifyId(list_marketplace.ToArray().Length - 1);
        AdjustScrollSize();

        if (name != null) marketplace.SetItems(name, comission, rate);
    }

    public void AddMarketplaces(MarketplaceItems[] arr_item)
    {
        for (int i = 0; i < arr_item.Length; i++)
            AddMarketplace(arr_item[i].name, arr_item[i].commission, arr_item[i].rate);

        AddMarketplace();
    }

    void AdjustScrollSize()
    {
        var scrollExtraSize = 25;

        scroll_marketplaces.content.sizeDelta = new Vector2((list_marketplace.ToArray().Length * productDistance) + scrollExtraSize, scroll_marketplaces.content.sizeDelta.y);
    }

    public void RemoveMarketplaceFromList(int id)
    {
        list_marketplace.RemoveAt(id);

        ModifyMarketplaceVariableValues();
        AdjustScrollSize();
    }

    void ModifyMarketplaceVariableValues()
    {
        for (int i = 0; i < list_marketplace.ToArray().Length; i++)
        {
            list_marketplace[i].ModifyId(i);
        }
    }

    void Calc()
    {
        if(!UtilityHelper.ValidateInputField(reqF_value.inputF, reqF_value.txt_warning)) return;
        if(!UtilityHelper.ValidateInputField(reqF_margin.inputF, reqF_margin.txt_warning)) return;
        
        var value = UtilityHelper.AdjustStringRealToFloat(reqF_value.inputF.text);
        var margin = UtilityHelper.AdjustStringRealToFloat(reqF_margin.inputF.text)/100;

        var lucro = value * margin;
        for (int i = 0; i < list_marketplace.ToArray().Length - 1; i++)
        {
            if(list_marketplace[i].inputF_commision.text == "")
                list_marketplace[i].inputF_commision.text = "0";

            if(list_marketplace[i].inputF_rate.text == "")
                list_marketplace[i].inputF_rate.text = "0";

            var comission = UtilityHelper.AdjustStringRealToFloat(list_marketplace[i].inputF_commision.text)/100;
            var rate = UtilityHelper.AdjustStringRealToFloat(list_marketplace[i].inputF_rate.text);

            var comission_value = (value + lucro) * comission;
            var price = value + lucro + comission_value + rate;

            list_marketplace[i].inputF_profit.text = $"R$ {lucro}";
            list_marketplace[i].inputF_price.text = $"R$ {price}";
        }

        margin = margin * 100;

        SaveData(value, margin);
    }

    void SaveData(float value, float margin)
    {
        List<MarketplaceItems> items = new List<MarketplaceItems>();
        for (int i = 0; i < list_marketplace.ToArray().Length - 1; i++)
        {
            string name = list_marketplace[i].inputF_name.text;
            float comission = UtilityHelper.AdjustStringRealToFloat(list_marketplace[i].inputF_commision.text);
            float rate = UtilityHelper.AdjustStringRealToFloat(list_marketplace[i].inputF_rate.text);

            items.Add(new MarketplaceItems(name, comission, rate));
        }

        string json_products = JsonHelper.ToJson<MarketplaceItems>(items);
        PlayerPrefs.SetString(NEW_MARKETPLACE_MARKETPLACES, json_products);

        PlayerPrefs.SetFloat(NEW_MARKETPLACE_VALUE, value);
        PlayerPrefs.SetFloat(NEW_MARKETPLACE_MARGIN, margin);
    }

}
